<?php

require_once("../PDOInterface.php");
require_once("../config.php");

session_start();
if(!isset($_SESSION['sessId']))
    header("Location: index.php");


    
try
{
    $dbc = new PDOInterface();
    $dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

    if(isset($_POST['title']))
    {
        //Gen and ADD
        $getVotingQuery = "INSERT INTO Voting VALUES(NULL,?,?,?)";
        $result = $dbc->getHandle()->prepare($getVotingQuery);

        $r = $result->execute([$_POST['title'], $_POST['start'], $_POST['end']]);
        
        $qid = $dbc->getHandle()->lastInsertId();
        header("Location: AddVoting.php?qId=".$qid);
    }
    else if(isset($_POST['addQTitle']))
    {
        $getVotingQuery = "INSERT INTO Question VALUES(NULL,?,?,?)";
        $result = $dbc->getHandle()->prepare($getVotingQuery);

        $r = $result->execute([$_POST['qid'], $_POST['addQTitle'], $_POST['pos']]);
        
        $qid = $dbc->getHandle()->lastInsertId();
    }
    else if(isset($_POST['addATitle']))
    {
        $getVotingQuery = "INSERT INTO Answer VALUES(NULL,?,?)";
        $result = $dbc->getHandle()->prepare($getVotingQuery);

        $r = $result->execute([$_POST['qid'], $_POST['addATitle']]);
        
        $qid = $dbc->getHandle()->lastInsertId();
    }
    else
    {
        $getVotingQuery = "SELECT * FROM Voting WHERE votingId=?";
        $result = $dbc->getHandle()->prepare($getVotingQuery);

        $r = $result->execute([$_GET['qId']]);
        $voting = $result->fetchAll()[0];

        //Get Questions with answers
        $getQuestionsWithAnswers = "SELECT * FROM Question q WHERE q.votingId=?";
        $result = $dbc->getHandle()->prepare($getQuestionsWithAnswers);
        $r = $result->execute([$_GET['qId']]);

        $questions = $result->fetchAll();
    }
    
} 
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}


?>
    <html>

    <head>
        <link rel="stylesheet" href="../layout.css">
        <link rel="stylesheet" href="../visual.css">
        <link rel="stylesheet" href="./adminstyle.css">

    </head>
    <script>
    function addQuestion(vid, pos)
    {
        var pytanko = prompt("Podaj treść pytania");
        if(pytanko != null)
        {
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", location, false);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("qid="+vid+"&addQTitle="+pytanko+"&pos="+pos);
            location.reload();
        }
    }

    function addAnswer(qid)
    {
        var odp = prompt("Podaj treść pytania");
        if(odp != null)
        {
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", location, false);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("qid="+qid+"&addATitle="+odp);
            location.reload();
        }
    }

    </script>
    
    <body>
    
    
        <?php require "../header.php";?>

        <center>
        <div class="content">

            <a href="index.php"><button>Cofnij</button></a>
            <br>
            <a href="logout.php"><button class="red">Wyloguj</button></a>
            <h1>Dodaj Głosowanie</h1>

            <?php

            if(isset($_GET['qId']))
            {
            ?>
            <h4>Nazwa głosowania: <?php echo $voting['votingName'];?></h4>
            <h4>Od: <?php echo $voting['startDate'];?></h4>
            <h4>Do: <?php echo $voting['endDate'];?></h4>
            <h3>Pytania</h3>
                <table>
                <thead>
                    <tr>
                        <td>Pytanie</td>
                        <td>Odpowiedzi</td>
                        <td>Akcje</td>
                    </tr>
                </thead>
            <?php
                foreach($questions as $value)
                {
                    //Get ansers
                    $getVotingQuery = "SELECT * FROM Answer WHERE questionId=?";
                    $result = $dbc->getHandle()->prepare($getVotingQuery);

                    $r = $result->execute([$value['questionId']]);
                    $answ = $result->fetchAll();

                    echo "<tr><td>".$value['questionTitle']."</td><td><table>";
                    
                    foreach($answ as $answer)
                    {
                        echo "<tr><td>".$answer['answerText']."</td><td>[X soon]</td></tr>";
                    }
                    echo "</table></td><td>";
                    ?>
                    <button onclick="addAnswer(<?php echo $value['questionId']; ?>)">Dodaj odpowiedź</button>
                    <?php
                    echo "</td></tr>";
                }

            ?>
            </table>
            <button onclick="addQuestion(<?php echo $_GET['qId']; ?>, <?php echo count($questions)+1; ?>)">Dodaj pytanie</button>
            <?php
            }
            else
            {
                ?>
                <form method="post" autocomplete="off">
                        <div style="margin-bottom: 10pt; height: auto;">
                        <p> Nazwa głosowania: </p><input type="text" name="title"> </input>
                        <p> Start: </p><input type="datetime-local" name="start"> </input>
                        <p> Koniec: </p><input type="datetime-local" name="end"> </input>
                        </div>
                        <div style="margin-top: 5pt; height: 20pt;">
                            <button type="submit"> Dodaj głosowanie</buton>
                        </div>
                    </form>

                    <?php
            }
            ?>
        </div>
    </center>

       


    </body>
    
    </html>
