<?php

require_once("../PDOInterface.php");
require_once("../config.php");

session_start();
if(!isset($_SESSION['sessId']))
    header("Location: index.php");


try
{
    $dbc = new PDOInterface();
    $dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

    $checkKeyQuery = "SELECT q.questionId as qid, questionTitle, COUNT(*) as ile FROM SentAnswers s JOIN Question q ON s.questionId = q.questionId WHERE s.votingId=? GROUP BY s.questionId";

    $result = $dbc->getHandle()->prepare($checkKeyQuery);
    $r = $result->execute([$_GET['id']]);

    $votingsCounts = $result->fetchAll();
} 
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}


?>
    <html>

    <head>
        <link rel="stylesheet" href="../layout.css">
        <link rel="stylesheet" href="../visual.css">
        <link rel="stylesheet" href="./adminstyle.css">

    </head>
    
    <body>
    
    
        <?php require "../header.php";?>

        <center>
        <div class="content">

            <a href="index.php"><button>Cofnij</button></a>
            <br>
            <a href="logout.php"><button class="red">Wyloguj</button></a>

            <h1>Wyniki głosowania:</h1>
        <?php

        foreach ($votingsCounts as $value)
        {
           echo "<h2>".$value['questionTitle']."</h2>";
           echo "<h3>Głosowało: ".$value['ile']."</h3>";
           echo "<h3>Odpowiedzi:</h3><ul>";
           $result = $dbc->getHandle()->prepare("SELECT a.answerText as answ , COUNT(*) as ile FROM SentAnswers s JOIN Answer a ON s.answerId = a.answerId WHERE s.questionId=? GROUP BY s.answerId");
           $r = $result->execute([$value['qid']]);
           $answers = $result->fetchAll();
           foreach($answers as $a)
           {
               echo "<li>".$a['answ']." - ".$a['ile']."</li>";
           }
           echo "</ul>";
        }

        ?>
            </table>


        </div>
    </center>

       


    </body>
    
    </html>
