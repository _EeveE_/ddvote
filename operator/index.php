<?php

session_start();
if(isset($_SESSION['sessId']))
    header("Location: ListVotings.php");

if(isset($_POST['key']))
{
    if(password_verify($_POST['key'], "")) //DEV ONLY
    {
        $_SESSION['sessId'] =  substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnoprstuvwxyz'),1, 128);
        header("Location: ListVotings.php");
    }
    else
    {
        echo "<script>alert('Invalid passwd!');</script>";
    }
}

?>
    <html>

    <head>
        <link rel="stylesheet" href="../layout.css">
        <link rel="stylesheet" href="../visual.css">
    </head>
    
    <body>
    
        <?php require "../header.php";?>
        <div class="keyform center">
            <div class="center">
                <center>
                    <p> Klucz operatora:
                        </p>
                    <form method="post" autocomplete="off">
                        <div style="margin-bottom: 10pt; height: 20pt;">
                            <input type="password" name="key"> </input>
                        </div>
                        <div style="margin-top: 5pt; height: 20pt;">
                            <button type="submit"> Zaloguj </buton>
                        </div>
                    </form>
                </center>
            </div>
        </div>
    </body>
    
    </html>
