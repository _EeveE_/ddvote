<?php

require_once("../PDOInterface.php");
require_once("../config.php");

session_start();
if(!isset($_SESSION['sessId']))
    header("Location: index.php");


try
{
    $dbc = new PDOInterface();
    $dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

    $checkKeyQuery = "SELECT * FROM Voting";

    $result = $dbc->getHandle()->prepare($checkKeyQuery);
    $r = $result->execute([]);

    $votings = $result->fetchAll();
} 
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}


?>
<html>

<head>
    <link rel="stylesheet" href="../layout.css">
    <link rel="stylesheet" href="../visual.css">
    <link rel="stylesheet" href="./adminstyle.css">
</head>

<body>

    <?php require "../header.php";?>



    <center>
        <div class="content">

            <a href="AddVoting.php"><button>Dodaj głosowanie</button></a>
            <br>
            <a href="logout.php"><button class="red">Wyloguj</button></a>

            <table class="sessionsPrint">
                <thead>
                    <tr>
                        <td>Sesja</td>
                        <td>Od</td>
                        <td>Do</td>
                        <td>Akcje</td>
                    </tr>
                </thead>
                <?php
        foreach ($votings as $value) 
        {
            echo "<tr><td>".$value['votingName']."</td><td>".$value['startDate']."</td><td>".$value['endDate']."</td><td><a href='Results.php?id=".$value['votingId']."'><button class='actions'>Wyniki</button></a> <a href='GenKeys.php?id=".$value['votingId']."'><button class='actions'>Generuj klucze</button></a></td></tr>";
        }
        ?>
            </table>


        </div>
    </center>
</body>


</html>