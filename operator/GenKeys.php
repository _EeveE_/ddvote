<?php

require_once("../PDOInterface.php");
require_once("../config.php");

session_start();
if(!isset($_SESSION['sessId']))
    header("Location: index.php");

if(!isset($_GET['id']) && !(isset($_POST['keyCount'])))
    header("Location: ListVotings.php");

try
{
    $dbc = new PDOInterface();
    $dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

    if(isset($_POST['keyCount']))
    {
        //Gen and ADD
        $getVotingQuery = "INSERT INTO VotingKeys VALUES(NULL,?,?)";
        $result = $dbc->getHandle()->prepare($getVotingQuery);

        for($i = 0; $i < $_POST['keyCount'];$i++)
        {
            $kluczu = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnoprstuvwxyz'),1, 128);
            $r = $result->execute([$_POST['id'], $kluczu]);
        }

        header("Location: GenKeys.php?id=".$_POST['id']);
    }
    else
    {
        $getVotingQuery = "SELECT * FROM VotingKeys WHERE votingId=?";
        $result = $dbc->getHandle()->prepare($getVotingQuery);
        $r = $result->execute([$_GET['id']]);
        $klucze = $result->fetchAll();
    }
   
} 
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}


?>
    <html>

    <head>
        <link rel="stylesheet" href="../layout.css">
        <link rel="stylesheet" href="../visual.css">
        <link rel="stylesheet" href="./adminstyle.css">

    </head>
    
    <body>
    
    
        <?php require "../header.php";?>

        <center>
        <div class="content">

            <a href="index.php"><button>Cofnij</button></a>
            <br>
            <a href="logout.php"><button class="red">Wyloguj</button></a>

            <h1>Generowanie kluczy</h1>


                <form method="post" autocomplete="off"> 
                <p>Ile nowych kluczy chcesz wygenerować?</p>
                        <div style="margin-bottom: 10pt; height: 20pt;">
                            <input type="text" name="keyCount"> </input>
                        </div>
                        <input type="hidden" value="<?php echo $_GET['id'];?>" name="id"></input>
                        <div style="margin-top: 5pt; height: 20pt;">
                            <button type="submit"> Generuj </buton>
                        </div>
                    </form>


        <h1>Niewykorzystane klucze</h1>
        <table>
        <thead>
                    <tr>
                        <td>Klucz</td>
                    </tr>
                </thead>
        <?php
        foreach ($klucze as $value)
        {
           echo "<tr><td>".$value['votingKey']."</td></tr>";
        }

        ?>
            </table>
            <a href="exportKeys.php?id=<?php echo $_GET['id']; ?>" target="_blank" rel="noopener noreferrer"><button>Eksport do CSV</button></a>
            


        </div>
    </center>

       


    </body>
    
    </html>
