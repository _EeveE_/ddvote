<?php

require_once("../PDOInterface.php");
require_once("../config.php");

session_start();
if(!isset($_SESSION['sessId']))
    header("Location: index.php");

if(!isset($_GET['id']))
    header("Location: ListVotings.php");

try
{
    $dbc = new PDOInterface();
    $dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

    $getVotingQuery = "SELECT * FROM VotingKeys WHERE votingId=?";
    $result = $dbc->getHandle()->prepare($getVotingQuery);
    $r = $result->execute([$_GET['id']]);
    $klucze = $result->fetchAll();

    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 01 Jan 2001 00:00:01 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-Type: text/x-csv');
    header("Content-Disposition: attachment;filename=keys.csv");
    header("Content-Transfer-Encoding: binary");
    header("Connection: close");
    
    foreach($klucze as $value)
    {
        echo $value['votingKey']."\r\n";
    }
    
   
} 
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}


?>

