<html>

<head>
    <link rel="stylesheet" href="layout.css">
    <link rel="stylesheet" href="visual.css">
</head>

<body>

<?php

require "./header.php";

require_once("PDOInterface.php");
require_once("Question.php");
require_once("config.php");

if(!isset($_POST['key']))
{
    echo "No voting key sent!";
    die();
}

$dbc = new PDOInterface();
$dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

try
{
    $checkKeyQuery = "SELECT * FROM VotingKeys WHERE votingKey=?";

    $result = $dbc->getHandle()->prepare($checkKeyQuery);
    $r = $result->execute([$_POST['key']]);

    if($result->rowCount() !== 1)
    {
        echo "Invalid key sent!";
        die();
    }

    $votings = $result->fetchAll();
    $voting = $votings[0]['votingId'];

    //Generate Session
    $sessKey = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnoprstuvwxyz'),1, 128);

    $insertSession = "INSERT INTO Sessions VALUES(NULL,?,?)";
    $result = $dbc->getHandle()->prepare($insertSession);
    $r = $result->execute([$voting, $sessKey]);



    //Get Voting
    $getVotingQuery = "SELECT * FROM Voting WHERE votingId=?";

    $result = $dbc->getHandle()->prepare($getVotingQuery);
    $r = $result->execute([$voting]);

    if($result->rowCount() !== 1)
    {
        echo "Internal Error!";
        die();
    }

    $votings = $result->fetchAll();

    //Check dates
    if(strtotime($votings[0]['startDate']) > time())
    {
        echo "Err too early";
        die();
    }

    if(strtotime($votings[0]['endDate']) < time())
    {
        echo "Err too late";
        die();
    }

        //TODO
    $removeKeyQuery = "DELETE FROM VotingKeys WHERE votingKey=?";
    $result = $dbc->getHandle()->prepare($removeKeyQuery);
    $r = $result->execute([$_POST['key']]);


    $title = $votings[0]['votingName'];


    //Get Questions with answers
    $getQuestionsWithAnswers = "SELECT * FROM Question q JOIN Answer a ON q.questionId = a.questionId WHERE q.votingId=?";
    $result = $dbc->getHandle()->prepare($getQuestionsWithAnswers);
    $r = $result->execute([$voting]);

    $qanda = $result->fetchAll();
    $questions = array();
    //Parse QandA
    for($i = 0; $i < $result->rowCount(); $i++)
    {
        $num = $qanda[$i]['questionNumber'];
        $questions[$num - 1]['title'] = $qanda[$i]['questionTitle'];
        $questions[$num - 1]['id'] = $qanda[$i]['questionId'];
        
        if(!array_key_exists('answers', $questions[$num - 1]))
            $questions[$num - 1]['answers'] = array();
      
        array_push($questions[$num - 1 ]['answers'], array($qanda[$i]['answerText'], $qanda[$i]['answerId']));
    }
    


    ?>
    <div class="wrapper"><form method="post" action="submitAnswer.php">
    <?php
    Question::questionsprint($title, $questions, $sessKey, $voting, $votings);
    
    ?>
    <input type="hidden" value="<?php echo $sessKey?>" name="sessKey"></input>
    <input type="hidden" value="<?php echo $voting?>" name="votingId"></input>
    <center><button type="submit">Zatwierdź</button></center>
    </form></div>
    <?php
    die();
    

}
catch(Exception $e)
{
    echo $e->getMessage();
    die();
}

$dbc->disconnect();


?>

</body>

</html>
