<html>

<head>
    <link rel="stylesheet" href="layout.css">
    <link rel="stylesheet" href="visual.css">
</head>

<body>


<?php

require "./header.php";

require_once("PDOInterface.php");
require_once("config.php");

if(!isset($_POST['votingId']) || !isset($_POST['sessKey']))
{
    echo "Data corruption found!";
    die();
}

$dbc = new PDOInterface();
$dbc->connect($dbHost, $dbUser, $dbPasswd, $dbName, $dbPort);

//Check SessKEy
$sessKeyCheck = "SELECT * FROM Sessions WHERE sessionKey=?";
$result = $dbc->getHandle()->prepare($sessKeyCheck);
$r = $result->execute([$_POST['sessKey']]);

if($result->rowCount() !== 1)
{
    echo "Invalid sesskey sent!";
    die();
}

$sessKeyCheck = "DELETE FROM Sessions WHERE sessionKey=?";
$result = $dbc->getHandle()->prepare($sessKeyCheck);
$r = $result->execute([$_POST['sessKey']]);


//Add to db

$insertQuery = "INSERT INTO SentAnswers VALUES(NULL,?,?,?)";

foreach($_POST as $key => $val)
{

    if(substr($key, 0, 8) != 'question')
        continue;

    $questionId  = substr($key, 8);
    

    $result = $dbc->getHandle()->prepare($insertQuery);
    $r = $result->execute([$questionId, $val, $_POST['votingId']]);

}



$dbc->disconnect();

echo '<div class="wrapper"><div class="questions">';
echo "<h1>Dziękujemy za udział w głosowaniu</h1>";
echo '</div></div>';

?>