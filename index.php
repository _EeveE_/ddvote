<html>

<head>
    <link rel="stylesheet" href="layout.css">
    <link rel="stylesheet" href="visual.css">
    <meta charset="utf-8">
    <title>System g&#322;osowania niejawnego WIiT - Logowanie</title>
    <link rel="shortcut icon" href="https://it.pk.edu.pl/favicon.ico" type="image/x-icon">
</head>

<body>

    <?php require "./header.php";?>
    <div class="keyform center">
        <div class="center">
            <center>
                <p> Klucz g&#322;osowania:
                    </p>
                <form action="questions.php" method="post" autocomplete="off">
                    <div style="margin-bottom: 10pt; height: 20pt;">
                        <input type="text" name="key"> </input>
                    </div>
                    <div style="margin-top: 5pt; height: 20pt;">
                        <button type="submit"> Zaloguj </buton>
                    </div>
                </form>
            </center>
        </div>
    </div>
</body>

</html>