<?php

class PDOInterface
{
	//Connect
	public function connect($host, $user, $password, $database, $port = 3306)
	{
		if($this->connected)
		{
			return;
		}
		
		try
		{
			$this->handle = new PDO("mysql:host=" . $host . ";port=".$port.";dbname=" . $database . ";", $user, $password);
			$this->handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$this->handle->exec("set names utf8");
		}
		catch (PDOException $e)
		{
			echo "Connection error: " . $e->getMessage();
			return;
		}

		$this->connected = true;
	}	

	//Get PDO Handle
	public function getHandle()
	{
		if(!$this->connected)
		{
			return;
		}

		return $this->handle;
	}

	//Disconnect
	public function disconnect()
	{	
		$this->handle = null; 
		$this->connected = false;
	}

	private $connected = false;
	private $handle = null;
}

?>