<?php
date_default_timezone_set('Europe/Warsaw');
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
class Question
{
	//Connect
	public static function questionsprint($title, $questions, $sesskey, $voting, $votings)
	{


        ?>
<div class="questions">

    <div class="sessionTitle">
        <h1>
            <?php echo $title?> </h1>
            <h3>Do końca głosowania pozostało:<br>
            <?php 
            $votingTimeNow = new DateTime('now');
            $votingTimeEnd = new DateTime($votings[0]['endDate']);
            $diff=date_diff($votingTimeNow, $votingTimeEnd);
            
            if ($diff->format('%d')>0)
            {
                echo '<timeDate id="days">'. $diff->format('%d dni, ') . '</timeDate>';
            }

            echo '<timeDate id="times">'. $diff->format('%H:%I:%S'). '</timeDate>';
            ?>
            </h3>
    </div>


        
<script>
var countDownDate = new Date("<?php echo $votingTimeEnd->format('M d, Y H:i:s')?>").getTime();

var x = setInterval(function() {

  var now = new Date().getTime();

  var distance = countDownDate - now;

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  if (days>0)
  {
    document.getElementById("times").innerHTML = days + "dni, "
  }

  document.getElementById("times").innerHTML = ("0" + hours).slice (-2) + ":"
  + ("0" + minutes).slice (-2) + ":" + ("0" + seconds).slice (-2);

  if (distance < 0) {
    clearInterval(x);
    window.location.href = 'expired.php';
  }
}, 500);
</script>




    <?php

        Question::qarray($questions);
    }	

    public static function printAnswers($ans, $id)
    {
        ?>
        <div class="answer">
        <label><input type="radio" name="<?php echo 'question'. $id?>" value="<?php echo $ans[1]?>">  <?php echo $ans[0]?> </input></label>
        </div> 
        <?php
    }

    public static function qarray($questions)
    {
        foreach ($questions as $q)
        {
            Question::singleQuestion($q['title'],$q['answers'],$q['id']);
        }
    }
    

    public static function singleQuestion($title, $answers, $id)
    {
         ?>
         <div class = "singleQuestion">
             <div class= "questionTitle">
    <h2>
        <?php echo $title?>
    </h2>
    </div>
    <hr>
    <fieldset id="<?php echo $id?>">
    <?php
        foreach ($answers as $a)
        {
            Question::printAnswers($a, $id);
        }
        ?>
        </fieldset>
    </div>


    <?php
    }
}
?>